//
//  DatabaseManager.swift
//  ParentsAndChildrensTask
//
//  Created by Nikolay Shelepov on 09.09.2020.
//  Copyright © 2020 Nikolay Shelepov. All rights reserved.
//

import Foundation
import GRDB

class DatabaseManager{
    
    static var migrator: DatabaseMigrator{
        var migrator = DatabaseMigrator()
        
        migrator.registerMigration("createParent"){ db in
            try db.create(table: "parent"){ t in
                t.autoIncrementedPrimaryKey("id")
                t.column("name", .text).notNull()
                t.column("surname", .text).notNull()
                t.column("middleName", .text).notNull()
                t.column("age", .integer).notNull()
                t.column("numberOfChildren", .integer).notNull()
                t.column("maxNumberOfChildren", .integer).notNull()
            }
        }
        
        migrator.registerMigration("createChildren"){ db in
            try db.create(table: "child"){ t in
                t.autoIncrementedPrimaryKey("id")
                t.column("name", .text).notNull()
                t.column("age", .integer).notNull()
                t.column("parent", .text).notNull()
            }
        }
        
        #if DEBUG
        migrator.eraseDatabaseOnSchemaChange = true
        #endif
        
        return migrator
    }
    
    static func setup(for application: UIApplication) throws{
        let databaseURL = try FileManager.default.url(for: .applicationDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("db.sqlite3")
        DBQueue.shared = try DatabaseQueue(path: databaseURL.path)
        print(databaseURL.path)
        try migrator.migrate(DBQueue.shared)
    }
}

