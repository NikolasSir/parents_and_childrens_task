//
//  Child.swift
//  ParentsAndChildrensTask
//
//  Created by Nikolay Shelepov on 09.09.2020.
//  Copyright © 2020 Nikolay Shelepov. All rights reserved.
//

import Foundation
import GRDB

struct Child{
    var id: Int64?
    var name: String
    var age: Int
    var parent: Parent
}

extension Child: Hashable {}

extension Child: Codable, FetchableRecord, MutablePersistableRecord{
    
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}
