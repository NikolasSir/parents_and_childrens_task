//
//  Parent.swift
//  ParentsAndChildrensTask
//
//  Created by Nikolay Shelepov on 09.09.2020.
//  Copyright © 2020 Nikolay Shelepov. All rights reserved.
//

import Foundation
import GRDB

struct Parent{
    var id: Int64?
    var name: String
    var surname: String
    var middleName: String
    var age: Int
    var numberOfChildren = 0
    let maxNumberOfChildren = 5
}

extension Parent: Hashable { }

extension Parent: Codable, FetchableRecord, MutablePersistableRecord{
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}
