//
//  ViewModel.swift
//  ParentsAndChildrensTask
//
//  Created by Nikolay Shelepov on 09.09.2020.
//  Copyright © 2020 Nikolay Shelepov. All rights reserved.
//

import Foundation
import GRDB

class ViewModel{
    
    private(set) var parent: Parent?
    var children = [Child]()
    var transactionObserver: TransactionObserver?
    
    func saveParent(name: String, surname: String, middleName: String, age: String) throws {
        do{
            try DBQueue.shared.write { db in
                var parent = Parent(name: name, surname: surname, middleName: middleName, age: Int(age)!)
                try! parent.insert(db)
                self.parent = parent
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
//    private func validateAlphabetSymbols(pattern: String) throws -> Bool{
//        do{
//            let alphabetRegex = try NSRegularExpression(pattern:"[А-Яа-я]", options: .caseInsensitive)
//        } catch let error{
//            print(error.localizedDescription)
//        }
//        
//    }
//    
//    private func validateNumbersSymbols(pattern: String) throws -> Bool{
//        do{
//            let numericRegex = try NSRegularExpression(pattern: "[0-9]{2}", options: .caseInsensitive)
//        } catch let error{
//            print(error.localizedDescription)
//        }
//    }
    
    func updateParent(_ name: String, _ surname: String, _ middleName: String, _ age: String, _ numberOfChildren: Int) throws{
        guard let parent = self.parent else { return }
        do{
            try DBQueue.shared.write { db in
                if var parentDB = try Parent.fetchOne(db, key: parent.id){
                    parentDB.name = name
                    parentDB.surname = surname
                    parentDB.middleName = middleName
                    parentDB.age = Int(age)!
                    parentDB.numberOfChildren = numberOfChildren
                    try parentDB.updateChanges(db, from: parent)
                }
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getParent() -> Parent?{
        var parent: Parent?
        do{
            try DBQueue.shared.read{ db in
                parent = try Parent.fetchOne(db)
            }
        } catch let error{
            print(error.localizedDescription)
        }
        return parent
    }
    
    func addChild(name: String, age: String) throws {
        guard let parent = self.parent else { return }
        var child = Child(name: name, age: Int(age)!, parent: parent)
        if child.parent.numberOfChildren < child.parent.maxNumberOfChildren {
            do{
                try DBQueue.shared.write { db in
                    child.parent.numberOfChildren += 1
                    self.parent?.numberOfChildren += 1
                    try! child.insert(db)
                    try self.parent?.update(db)
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func deleteChild(key: Int64) throws {
        do{
            _ = try DBQueue.shared.write { db in
                try Child.deleteOne(db, key: key)
                self.parent?.numberOfChildren -= 1
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getChildrenCount() -> (count: Int, error: Error?){
        var count = 0
        do{
            count = try DBQueue.shared.read{ db in
                try Child.fetchCount(db)
            }
        } catch let error{
            print(error.localizedDescription)
            return (count, error)
        }
        return (count, nil)
    }
    
    func isChildrenEmpty() -> (condition: Bool, error: Error?){
        var count = 0
        do{
            count = try DBQueue.shared.read{ db in
                try Child.fetchCount(db)
            }
        } catch let error{
            print(error.localizedDescription)
            return (true, error)
        }
        return (count == 0, nil)
    }
    
    func getTransactionObserver(onError: @escaping (Error) -> Void, onChange: @escaping ([Child]) -> Void) -> TransactionObserver{
        return ValueObservation.tracking(value: Child.all().fetchAll).start(in: DBQueue.shared, onError: onError, onChange: onChange)
    }
}
