//
//  ViewController.swift
//  ParentsAndChildrensTask
//
//  Created by Nikolay Shelepov on 08.09.2020.
//  Copyright © 2020 Nikolay Shelepov. All rights reserved.
//

import UIKit
import GRDB

class ViewController: UIViewController {
    
    @IBOutlet weak var surname: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var middleName: UITextField!
    @IBOutlet weak var age: UITextField!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var add: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var viewModel = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if (viewModel.isChildrenEmpty().condition){
            tableView.isHidden = true
            name.text = viewModel.getParent()?.name ?? ""
            surname.text = viewModel.getParent()?.surname ?? ""
            middleName.text = viewModel.getParent()?.middleName ?? ""
            if let parent = viewModel.getParent(){
                age.text = String(parent.age)
            } else {
                age.text = ""
            }
        }
        //tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        viewModel.transactionObserver = viewModel.getTransactionObserver(onError: { error in
            print(error.localizedDescription)
        }, onChange: { childrenArray in
            self.updateTableView(children: childrenArray)
        })
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.transactionObserver = nil
    }
    
    @IBAction func saveOrEditData(_ sender: Any) {
        guard let name = name.text,
              let surname = surname.text,
              let middleName = middleName.text,
              let age = age.text else { return }
        if (save.titleLabel?.text == "Сохранить"){
            if let parent = viewModel.parent{
                do{
                    try viewModel.updateParent(name, surname, middleName, age, parent.numberOfChildren)
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                do{
                    try viewModel.saveParent(name: name, surname: surname, middleName: middleName, age: age)
                } catch {
                    print(error.localizedDescription)
                }
            }
            self.name.isUserInteractionEnabled = false
            self.name.backgroundColor = .lightGray
            self.surname.isUserInteractionEnabled = false
            self.surname.backgroundColor = .lightGray
            self.middleName.isUserInteractionEnabled = false
            self.middleName.backgroundColor = .lightGray
            self.age.isUserInteractionEnabled = false
            self.age.backgroundColor = .lightGray
            save.setTitle("Редактировать", for: .normal)
        } else if (save.titleLabel?.text == "Редактировать"){
            self.name.isUserInteractionEnabled = true
            self.name.backgroundColor = .clear
            self.surname.isUserInteractionEnabled = true
            self.surname.backgroundColor = .clear
            self.middleName.isUserInteractionEnabled = true
            self.middleName.backgroundColor = .clear
            self.age.isUserInteractionEnabled = true
            self.age.backgroundColor = .clear
            save.setTitle("Сохранить", for: .normal)
        }
    }
    
    @IBAction func addChild(_ sender: Any) {
        let alert = UIAlertController(title: "Ребенок", message: "Заполните свои данные", preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = "Напишите имя ребенка"
        }
        alert.addTextField { textField in
            textField.placeholder = "Напишите возраст ребенка"
        }
        alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: { alertAction in
            let textFieldOne = alert.textFields![0] as UITextField
            let textFieldTwo = alert.textFields![1] as UITextField
            do{
                try self.viewModel.addChild(name: textFieldOne.text!, age: textFieldTwo.text!)
            } catch let error{
                print(error.localizedDescription)
            }
            self.tableView.isHidden = false
            self.updateTableView(children: self.viewModel.children)
            if (self.viewModel.parent?.numberOfChildren == self.viewModel.parent?.maxNumberOfChildren){
                self.add.isHidden = true
            } else {
                self.add.isHidden = false
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default){ alertAction in })
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func deleteChild(sender: UIButton) throws{
        try self.viewModel.deleteChild(key: Int64(sender.tag))
        updateTableView(children: viewModel.children)
    }
    
    func updateTableView(children: [Child]){
        self.viewModel.children = children
        self.tableView.reloadData()
        if viewModel.isChildrenEmpty().condition {
            tableView.isHidden = true
        } else {
            tableView.isHidden = false
        }
        if let parent = viewModel.parent{
            if parent.numberOfChildren == parent.maxNumberOfChildren{
                add.isHidden = true
            } else {
                add.isHidden = false
            }
        }
    }
}

extension ViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.children.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? TableViewCell{
            cell.labelOne.text = viewModel.children[indexPath.row].name
            cell.labelTwo.text = String(viewModel.children[indexPath.row].age)
            cell.delete.tag = Int(viewModel.children[indexPath.row].id!)
            cell.delete.addTarget(self, action: #selector(deleteChild(sender:)), for: .touchUpInside)
            return cell
        }
        return UITableViewCell()
    }
}

